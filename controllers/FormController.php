<?php

namespace app\controllers;

use app\models\DataSet;
use app\models\DHISTransport;
use yii\helpers\Url;

class FormController extends \yii\web\Controller
{
    public function actionHivPaediatricMonthlySummary()
    {
        return $this->renderDataSetForm('EDzMBk0RRji');
    }

    public function actionMnchQuarterlyReport()
    {
        return $this->renderDataSetForm('EKWVBc5C0ms');
    }

    public function actionReproductiveHealth()
    {
        return $this->renderDataSetForm('QX4ZTUbOt3a');
    }

    private function renderDataSetForm($id){
        $dataSet = DataSet::find()->where(['id'=>$id])->one();
        if($dataSet)
            return $this->render('data-form', [
                'dataSet'=>$dataSet,
            ]);
        else{
            $result = DHISTransport::fetch('https://apps.dhis2.org/demo/api/dataSets/'.$id);
            if($result['code']==200){
                $data = json_decode($result['response']);
                DataSet::saveFromDHIS($data);
            }
            return $this->redirect(\Yii::$app->request->url);
        }
    }

}
