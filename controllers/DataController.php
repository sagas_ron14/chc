<?php

namespace app\controllers;

use app\models\DataSet;
use app\models\DHISObjectSearch;
use app\models\DHISTransport;
use Yii;
use yii\helpers\Url;

class DataController extends \yii\web\Controller
{
    public function actionChildHealthFacilities()
    {
        $searchModel = new DHISObjectSearch();
        $dataProvider = $searchModel->childHealth(Yii::$app->request->queryParams);

        return $this->render('child-health-facilities', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRefreshChildHealthData(){

        $result = DHISTransport::fetch('https://apps.dhis2.org/demo/api/dataSets/BfMAe6Itzgt');
        if($result['code']==200){
            $data = json_decode($result['response']);
            DataSet::saveFromDHIS($data);
        }
        header('Refresh: 3;url='.Url::to(['/data/child-health-facilities']));

        return $this->renderContent('Fetching data<br>Please wait a few seconds...');

    }

}
