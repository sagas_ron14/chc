<?php
/* @var $this yii\web\View */
use yii\grid\GridView;

/* @var $searchModel app\models\DHISObjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Facilities with Child Health';
$this->params['breadcrumbs'][] = $this->title;
?>
<h2><?=$this->title?></h2>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'name:ntext',
        'code:ntext',
    ],
]); ?>

<?php if(empty($dataProvider->models)):?>
    <?=\yii\helpers\Html::a('<i class="glyphicon glyphicon-globe"></i> Update this List',
        ['refresh-child-health-data'],
        ['class'=>'btn btn-success col-md-offset-4 col-md-4'])?>
<?php endif?>
