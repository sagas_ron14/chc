<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <p>This is an application that achieves the following objectives in PHP</p>
    <ul>
        <li>Output a list of facilities that collect <?=Html::a('Child Health', ['/data/child-health-facilities'])?> information</li>
        <li>Output the forms for the following datasets.
            <ul>
                <li><?=Html::a('HIV Paediatric Monthly Summary', ['/form/hiv-paediatric-monthly-summary'])?></li>
                <li><?=Html::a('Reproductive Health', ['/form/reproductive-health'])?></li>
                <li><?=Html::a('MNCH Quarterly Report', ['/form/mnch-quarterly-report'])?></li>
            </ul>
        </li>
    </ul>
</div>
