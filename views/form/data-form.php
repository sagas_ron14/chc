<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataSet app\models\DataSet */
/* @var $form ActiveForm */
$this->title = $dataSet->displayName;
$this->params['breadcrumbs'][] = ['label'=>'Forms', 'url'=>'#'];
$this->params['breadcrumbs'][] = $this->title;
?>



<h2><?=$this->title?> Form</h2>

<div class="form-data-form col-md-8">

    <?php Html::beginForm(); ?>

        <?php foreach ($dataSet->getForm() as $element): ?>
            <div class="form-group">
                <?=Html::label($element['label'])?>
                <?=Html::textInput($element['name'], '', $element['options'])?>
            </div>

        <?php endforeach?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php Html::endForm(); ?>

</div><!-- form-data-form -->
