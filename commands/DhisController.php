<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\DataSet;
use app\models\DHISObject;
use app\models\DHISTransport;
use app\models\OrganisationUnit;
use yii\console\Controller;

/**
 * This command performs long DHIS data fetching tasks.
 */
class DhisController extends Controller
{
    /**
     * This command creates an organisation unit (if it does not exist) from the url supplied.
     * @param string $url the url from which to get the object
     */
    public function actionGetOrganisationUnit($url){
        $result = DHISTransport::fetch($url, true);
        if($result['code']==200){
            $data = json_decode($result['response']);
            OrganisationUnit::saveFromDHIS($data);
        }
    }

    /**
     * This command creates an organisation unit (if it does not exist) from the url supplied, then populates related objects.
     * @param string $url the url from which to get the object
     */
    public function actionGetDataSet($url){
        $result = DHISTransport::fetch($url, true);
        if($result['code']==200){
            $data = json_decode($result['response']);
            DataSet::saveFromDHIS($data);
        }
    }
}
