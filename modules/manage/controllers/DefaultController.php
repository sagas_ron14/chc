<?php

namespace app\modules\manage\controllers;

use app\models\DHISTransport;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTest(){
        $url = 'https://apps.dhis2.org/demo/api/dataSets/EDzMBk0RRji';
        header('Content-type: application/json');
        echo DHISTransport::fetch($url)['response'];
        \Yii::$app->end();
    }
}
