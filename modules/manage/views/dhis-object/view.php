<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DHISObject */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Dhisobjects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dhisobject-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->object_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->object_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'object_id',
            'id:ntext',
            'name:ntext',
            'code:ntext',
            'created:ntext',
            'lastUpdated:ntext',
            'href:ntext',
        ],
    ]) ?>

</div>
