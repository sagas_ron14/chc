<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DHISObject */

$this->title = 'Create Dhisobject';
$this->params['breadcrumbs'][] = ['label' => 'Dhisobjects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dhisobject-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
