<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DHISObjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'DHIS Objects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dhisobject-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= (!count($dataProvider->models))?Html::a('Import Defaults', ['import-default'], ['class' => 'btn btn-success']):''?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'object_id',
            'id:ntext',
            'name:ntext',
            'code:ntext',
            'created:ntext',
            'lastUpdated:ntext',
            'href:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
