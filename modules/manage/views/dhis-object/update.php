<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DHISObject */

$this->title = 'Update Dhisobject: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Dhisobjects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->object_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dhisobject-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
