<?php use yii\helpers\Html;?>
<div class="manage-default-index">
    <h2>Manage Data Sources</h2>
    <p><i class="glyphicon glyphicon-globe"></i> <?=Html::a('DHIS Objects', ['/manage/dhis-object/'])?></p>
    <p><i class="glyphicon glyphicon-cloud"></i> <?=Html::a('DataSet', ['/manage/data-set/'])?></p>
    <p><i class="glyphicon glyphicon-home"></i> <?=Html::a('Organisations', ['/manage/organisation-unit/'])?></p>
</div>
