<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrganisationUnitSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="organisation-unit-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'object_id') ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'parent_id') ?>

    <?= $form->field($model, 'code') ?>

    <?= $form->field($model, 'uuid') ?>

    <?php // echo $form->field($model, 'shortName') ?>

    <?php // echo $form->field($model, 'openingDate') ?>

    <?php // echo $form->field($model, 'displayName') ?>

    <?php // echo $form->field($model, 'displayShortName') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
