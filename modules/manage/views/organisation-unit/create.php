<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrganisationUnit */

$this->title = 'Create Organisation Unit';
$this->params['breadcrumbs'][] = ['label' => 'Organisation Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organisation-unit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
