<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrganisationUnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Organisation Units';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organisation-unit-index">

    <h2><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'object_id',
            'id:ntext',
            'parent_id:ntext',
            'code:ntext',
            'uuid:ntext',
            // 'shortName:ntext',
            // 'openingDate:ntext',
            // 'displayName:ntext',
            // 'displayShortName:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
