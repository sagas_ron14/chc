<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DataSetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Sets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-set-index">

    <h2><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'object_id',
            'id:ntext',
            'code:ntext',
            'shortName:ntext',
            'displayName:ntext',
            // 'displayShortName:ntext',
            // 'periodType:ntext',
            // 'legend_set_id:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
