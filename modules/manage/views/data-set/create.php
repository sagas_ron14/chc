<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DataSet */

$this->title = 'Create Data Set';
$this->params['breadcrumbs'][] = ['label' => 'Data Sets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-set-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
