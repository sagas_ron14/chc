<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DataSet */

$this->title = $model->object_id;
$this->params['breadcrumbs'][] = ['label' => 'Data Sets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-set-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->object_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->object_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'object_id',
            'id:ntext',
            'code:ntext',
            'shortName:ntext',
            'displayName:ntext',
            'displayShortName:ntext',
            'periodType:ntext',
            'legend_set_id:ntext',
        ],
    ]) ?>

</div>
