<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DataSet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-set-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'code')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'shortName')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'displayName')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'displayShortName')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'periodType')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'legend_set_id')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
