<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DataSet */

$this->title = 'Update Data Set: ' . ' ' . $model->object_id;
$this->params['breadcrumbs'][] = ['label' => 'Data Sets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->object_id, 'url' => ['view', 'id' => $model->object_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="data-set-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
