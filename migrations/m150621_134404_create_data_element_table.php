<?php

use yii\db\Schema;
use yii\db\Migration;

class m150621_134404_create_data_element_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('data_element', [
            'object_id'=>'int primary key auto_increment',
            'id'=>'text',
            'zeroIsSignificant'=>'text',
            'displayDescription'=>'text',
            'type'=>'text',
            'dataDimension'=>'text',
            'description'=>'text',
            'externalAccess'=>'text',
            'publicAccess'=>'text',
            'aggregationOperator'=>'text',
            'numberType'=>'text',
            'domainType'=>'text',
            'dimension'=>'text',
            'optionSetValue'=>'text',
            'displayName'=>'text',
            'displayShortName'=>'text',
            'category_combo_id'=>'text',
            'user_id'=>'text',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('data_element');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
