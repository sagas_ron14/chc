<?php

use yii\db\Schema;
use yii\db\Migration;

class m150621_130927_create_organisation_unit_group_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('organisation_unit_group', [
            'object_id'=>'int primary key auto_increment',
            'group_id'=>'text',
            'group_set_id'=>'text',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('organisation_unit_group');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
