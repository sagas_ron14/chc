<?php

use yii\db\Schema;
use yii\db\Migration;

class m150621_140638_create_data_element_group_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('data_element_group',[
            'object_id'=>'int primary key auto_increment',
            'data_element_group_set_id'=>'text',
            'shortName'=>'text',
            'displayName'=>'text',
            'displayShortName'=>'text',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('data_element_group');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
