<?php

use yii\db\Schema;
use yii\db\Migration;

class m150621_140444_create_data_set_data_elements_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('data_set_data_elements',[
            'object_id'=>'int primary key auto_increment',
            'code'=>'text',
            'shortName'=>'text',
            'displayName'=>'text',
            'displayShortName'=>'text',
            'periodType'=>'text',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('data_set_data_elements');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
