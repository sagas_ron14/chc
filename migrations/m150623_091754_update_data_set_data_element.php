<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_091754_update_data_set_data_element extends Migration
{
    public function safeUp()
    {
        $this->addColumn('data_set_data_elements', 'dataSet_id', 'text');
        $this->addColumn('data_set_data_elements', 'dataElement_id', 'text');

        $this->dropColumn('data_set_data_elements', 'code');
        $this->dropColumn('data_set_data_elements', 'shortName');
        $this->dropColumn('data_set_data_elements', 'displayName');
        $this->dropColumn('data_set_data_elements', 'displayShortName');
        $this->dropColumn('data_set_data_elements', 'periodType');
    }

    public function safeDown()
    {
        $this->dropColumn('data_set_data_elements', 'dataSet_id');
        $this->dropColumn('data_set_data_elements', 'dataElement_id');

        $this->addColumn('data_set_data_elements', 'code', 'text');
        $this->addColumn('data_set_data_elements', 'shortName', 'text');
        $this->addColumn('data_set_data_elements', 'displayName', 'text');
        $this->addColumn('data_set_data_elements', 'displayShortName', 'text');
        $this->addColumn('data_set_data_elements', 'periodType', 'text');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
