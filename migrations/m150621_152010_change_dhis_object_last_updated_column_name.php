<?php

use yii\db\Schema;
use yii\db\Migration;

class m150621_152010_change_dhis_object_last_updated_column_name extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('dhis_object', 'last_updated', 'lastUpdated');
    }

    public function safeDown()
    {
        $this->renameColumn('dhis_object', 'lastUpdated', 'last_updated');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
