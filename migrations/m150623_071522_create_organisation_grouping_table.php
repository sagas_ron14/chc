<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_071522_create_organisation_grouping_table extends Migration
{
    public function up()
    {
        $this->createTable('organisation_unit_grouping', [
            'object_id'=>'int primary key auto_increment',
            'unit_id'=>'text',
            'group_id'=>'text',
        ]);
    }

    public function down()
    {
        $this->dropTable('organisation_unit_grouping');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
