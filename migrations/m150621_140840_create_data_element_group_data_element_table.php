<?php

use yii\db\Schema;
use yii\db\Migration;

class m150621_140840_create_data_element_group_data_element_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('data_element_group_data_element',[
            'object_id'=>'int primary key auto_increment',
            'data_element_group_id'=>'text',
            'data_element_id'=>'text',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('data_element_group_data_element');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
