<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_073140_update_organisation_unit_group_table extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('organisation_unit_group', 'group_id', 'id');
        $this->addColumn('organisation_unit_group', 'shortName', 'text');
        $this->addColumn('organisation_unit_group', 'displayName', 'text');
        $this->addColumn('organisation_unit_group', 'displayShortName', 'text');
    }

    public function safeDown()
    {
        $this->renameColumn('organisation_unit_group', 'id', 'group_id');
        $this->dropColumn('organisation_unit_group', 'shortName');
        $this->dropColumn('organisation_unit_group', 'displayName');
        $this->dropColumn('organisation_unit_group', 'displayShortName');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
