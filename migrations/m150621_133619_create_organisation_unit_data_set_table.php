<?php

use yii\db\Schema;
use yii\db\Migration;

class m150621_133619_create_organisation_unit_data_set_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('organisation_unit_data_set', [
            'object_id'=>'int primary key auto_increment',
            'data_set_id'=>'text',
            'organisation_unit_id'=>'text',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('organisation_unit_data_set');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
