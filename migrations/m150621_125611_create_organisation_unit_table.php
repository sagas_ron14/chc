<?php

use yii\db\Schema;
use yii\db\Migration;

class m150621_125611_create_organisation_unit_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('organisation_unit', [
            'object_id' => 'int primary key auto_increment',
            'id' => 'text',
            'parent_id' => 'text',
            'code' => 'text',
            'uuid' => 'text',
            'shortName' => 'text',
            'openingDate' => 'text',
            'displayName' => 'text',
            'displayShortName' => 'text',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('organisation_unit');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
