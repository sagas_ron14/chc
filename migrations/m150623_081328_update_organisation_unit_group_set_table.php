<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_081328_update_organisation_unit_group_set_table extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('organisation_unit_group_set', 'set_id', 'id');
        $this->renameColumn('organisation_unit_group_set', 'unit_id', 'group_id');

    }

    public function safeDown()
    {
        $this->renameColumn('organisation_unit_group_set', 'id', 'set_id');
        $this->renameColumn('organisation_unit_group_set', 'group_id', 'unit_id');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
