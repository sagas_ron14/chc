<?php

use yii\db\Schema;
use yii\db\Migration;

class m150621_120123_create_dhis_object_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('dhis_object', [
            'object_id'=>'int primary key auto_increment',
            'id'=>'text',
            'name'=>'text',
            'code'=>'text',
            'created'=>'text',
            'last_updated'=>'text',
            'href'=>'text',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('dhis_object');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
