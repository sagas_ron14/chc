<?php

use yii\db\Schema;
use yii\db\Migration;

class m150621_131348_create_organisation_unit_group_set_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('organisation_unit_group_set',[
            'object_id'=>'int primary key auto_increment',
            'set_id'=>'text',
            'unit_id'=>'text',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('organisation_unit_group_set');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
