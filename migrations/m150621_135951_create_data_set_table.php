<?php

use yii\db\Schema;
use yii\db\Migration;

class m150621_135951_create_data_set_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('data_set',[
            'object_id'=>'int primary key auto_increment',
            'id'=>'text',
            'code'=>'text',
            'shortName'=>'text',
            'displayName'=>'text',
            'displayShortName'=>'text',
            'periodType'=>'text',
            'legend_set_id'=>'text',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('data_set');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
