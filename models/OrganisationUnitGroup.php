<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organisation_unit_group".
 *
 * @property integer $object_id
 * @property string $id
 * @property string $group_set_id
 * @property string $shortName
 * @property string $displayName
 * @property string $displayShortName
 */
class OrganisationUnitGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organisation_unit_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'group_set_id', 'shortName', 'displayName', 'displayShortName'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'object_id' => 'Object ID',
            'id' => 'ID',
            'group_set_id' => 'Group Set ID',
            'shortName' => 'Short Name',
            'displayName' => 'Display Name',
            'displayShortName' => 'Display Short Name',
        ];
    }

    public static function saveFromDHISArray($organisationUnitGroupArray)
    {
        if(null == self::find()->where(['id'=>$organisationUnitGroupArray['id']])->one()){
            $result = DHISTransport::fetch($organisationUnitGroupArray['href'], true);
            if($result['code']==200){
                $data = json_decode($result['response']);
                self::saveFromDHIS($data);
            }
        }
    }

    public static function saveFromDHIS($data){
        $group = new OrganisationUnitGroup();
        $group->id = $data->id;

        if(null == self::find()->where(['id'=>$data->id])->one()){
            $group->attributes = (array)$data;
            $group->save();
            DHISObject::saveObject((array)$data);
        }

        if(isset($data->organisationUnitGroupSet))
            OrganisationUnitGroupSet::addGroup($group->id, (array)$data->organisationUnitGroupSet);

        if(isset($data->organisationUnits)){
            foreach($data->organisationUnits as $organisationUnit)
                OrganisationUnit::saveFromDHIS($organisationUnit);
        }
    }
}
