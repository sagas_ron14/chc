<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_element".
 *
 * @property integer $object_id
 * @property string $id
 * @property string $zeroIsSignificant
 * @property string $displayDescription
 * @property string $type
 * @property string $dataDimension
 * @property string $description
 * @property string $externalAccess
 * @property string $publicAccess
 * @property string $aggregationOperator
 * @property string $numberType
 * @property string $domainType
 * @property string $dimension
 * @property string $optionSetValue
 * @property string $displayName
 * @property string $displayShortName
 * @property string $category_combo_id
 * @property string $user_id
 */
class DataElement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_element';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'zeroIsSignificant', 'displayDescription', 'type', 'dataDimension', 'description', 'externalAccess', 'publicAccess', 'aggregationOperator', 'numberType', 'domainType', 'dimension', 'optionSetValue', 'displayName', 'displayShortName', 'category_combo_id', 'user_id'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'object_id' => 'Object ID',
            'id' => 'ID',
            'zeroIsSignificant' => 'Zero Is Significant',
            'displayDescription' => 'Display Description',
            'type' => 'Type',
            'dataDimension' => 'Data Dimension',
            'description' => 'Description',
            'externalAccess' => 'External Access',
            'publicAccess' => 'Public Access',
            'aggregationOperator' => 'Aggregation Operator',
            'numberType' => 'Number Type',
            'domainType' => 'Domain Type',
            'dimension' => 'Dimension',
            'optionSetValue' => 'Option Set Value',
            'displayName' => 'Display Name',
            'displayShortName' => 'Display Short Name',
            'category_combo_id' => 'Category Combo ID',
            'user_id' => 'User ID',
        ];
    }
}
