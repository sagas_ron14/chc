<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_element_group_data_element".
 *
 * @property integer $object_id
 * @property string $data_element_group_id
 * @property string $data_element_id
 */
class DataElementGroupDataElement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_element_group_data_element';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_element_group_id', 'data_element_id'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'object_id' => 'Object ID',
            'data_element_group_id' => 'Data Element Group ID',
            'data_element_id' => 'Data Element ID',
        ];
    }
}
