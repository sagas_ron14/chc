<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organisation_unit_grouping".
 *
 * @property integer $object_id
 * @property string $unit_id
 * @property string $group_id
 */
class OrganisationUnitGrouping extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organisation_unit_grouping';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unit_id', 'group_id'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'object_id' => 'Object ID',
            'unit_id' => 'Unit ID',
            'group_id' => 'Group ID',
        ];
    }

    public static function record($unit_id, $organisationUnitGroup)
    {
        $setData = [ 'group_id'=>$organisationUnitGroup->id, 'unit_id'=>$unit_id];
        if(null == self::find()->where($setData)->one()){
            $grouping = new self;
            $grouping->attributes = $setData;
            $grouping->save();
            OrganisationUnitGroup::saveFromDHISArray((array)$organisationUnitGroup);
        };
    }
}
