<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_set_data_elements".
 *
 * @property integer $object_id
 * @property string $dataSet_id
 * @property string $dataElement_id
 *
 * @property string $dhisDataElement
 */
class DataSetDataElement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_set_data_elements';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dataSet_id', 'dataElement_id'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'object_id' => 'Object ID',
            'dataSet_id' => 'Data Set ID',
            'dataElement_id' => 'Data Element ID',
        ];
    }

    public static function record($dataSet_id, $dataElement)
    {
        $data = [ 'dataSet_id'=>$dataSet_id, 'dataElement_id'=>$dataElement->id];
        if(null == self::find()->where($data)->one()){
            $setElement = new self;
            $setElement->attributes = $data;
            $setElement->save();
            DHISObject::saveObject((array)$dataElement);
            //DataElement::saveFromDHIS($dataElement);
        };
    }

    public function getDhisDataElement()
    {
        return $this->hasOne(DHISObject::className(), ['id' => 'dataElement_id']);
    }

}
