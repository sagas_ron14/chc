<?php

namespace app\models;

use Yii;
use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "data_set".
 *
 * @property integer $object_id
 * @property string $id
 * @property string $code
 * @property string $shortName
 * @property string $displayName
 * @property string $displayShortName
 * @property string $periodType
 * @property string $legend_set_id
 */
class DataSet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_set';
    }

    public static function saveFromDHIS($data)
    {
        $dataSet = new DataSet();
        $dataSet->id = $data->id;

        if(null == self::find()->where(['id'=>$data->id])->one()){
            $dataSet->attributes = (array)$data;
            $dataSet->save();
            DHISObject::saveObject((array)$data);
        }

        if(isset($data->dataElements)){
            foreach($data->dataElements as $dataElement)
                DataSetDataElement::record($dataSet->id, $dataElement);
        }

        if(isset($data->organisationUnits)) {
            foreach ($data->organisationUnits as $unit) {
                $setInfo = ['data_set_id' => $dataSet->id, 'organisation_unit_id' => $unit->id];
                if (null == OrganisationUnitDataSet::find()->where($setInfo)->one()) {
                    $grouping = new OrganisationUnitDataSet();
                    $grouping->attributes = $setInfo;
                    $grouping->save();
                    DHISObject::saveObject((array)$unit);
                }
            }
        }
    }

    public static function saveFromDHISArray($dataSetArray)
    {
        if(null == self::find()->where(['id'=>$dataSetArray->id])->one()){
            $result = DHISTransport::fetch($dataSetArray->href, true);
            if($result['code']==200){
                $data = json_decode($result['response']);
                self::saveFromDHIS($data);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'code', 'shortName', 'displayName', 'displayShortName', 'periodType', 'legend_set_id'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'object_id' => 'Object ID',
            'id' => 'ID',
            'code' => 'Code',
            'shortName' => 'Short Name',
            'displayName' => 'Display Name',
            'displayShortName' => 'Display Short Name',
            'periodType' => 'Period Type',
            'legend_set_id' => 'Legend Set ID',
        ];
    }

    public function getForm()
    {
        $form = [];

        $data =  $dataElements = DataSetDataElement::find()->where([
            'dataSet_id'=>$this->id
        ])->all();

        $modelName = preg_replace('/\s+/', '', $this->displayName);

        if(!empty($data)){
            foreach($data as $field)
                $form[] = [
                    'type'=>'text',
                    'label'=>$field->dhisDataElement->name,
                    'name'=>preg_replace('/\s+/', '_', "$modelName"."[{$field->dhisDataElement->name}]"),
                    'options'=>['class'=>'form-control', 'id'=>$field->dataElement_id]
                ];
        }

        return $form;

    }
}
