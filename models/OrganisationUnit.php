<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organisation_unit".
 *
 * @property integer $object_id
 * @property string $id
 * @property string $parent_id
 * @property string $code
 * @property string $uuid
 * @property string $shortName
 * @property string $openingDate
 * @property string $displayName
 * @property string $displayShortName
 */
class OrganisationUnit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organisation_unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'code', 'uuid', 'shortName', 'openingDate', 'displayName', 'displayShortName'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'object_id' => 'Object ID',
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'code' => 'Code',
            'uuid' => 'Uuid',
            'shortName' => 'Short Name',
            'openingDate' => 'Opening Date',
            'displayName' => 'Display Name',
            'displayShortName' => 'Display Short Name',
        ];
    }

    /**
     * @param $data
     * @param bool $dataOnly
     */
    public static function saveFromDHIS($data, $dataOnly = false)
    {
        $unit = new OrganisationUnit();
        $unit->id = $data->id;

        if(isset($data->parent) && ($data->parent != null)){
            $unit->parent_id = $data->parent->id;
            DHISObject::saveObject((array)$data->parent);
        }

        if(null == self::find()->where(['id'=>$data->id])->one()){
            $unit->attributes = (array)$data;
            $unit->save();
            DHISObject::saveObject((array)$data);
        }

        if($dataOnly) return;

        if(isset($data->organisationUnitGroups)){
            foreach($data->organisationUnitGroups as $organisationUnitGroup)
                OrganisationUnitGrouping::record($unit->id, $organisationUnitGroup);
        }

        if(isset($data->dataSets)){
            foreach($data->dataSets as $dataSet)
                OrganisationUnitDataSet::record($unit->id, $dataSet);
        }
    }
}
