<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DHISObject;

/**
 * DHISObjectSearch represents the model behind the search form about `app\models\DHISObject`.
 */
class DHISObjectSearch extends DHISObject
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id'], 'integer'],
            [['id', 'name', 'code', 'created', 'lastUpdated', 'href'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DHISObject::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'object_id' => $this->object_id,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'created', $this->created])
            ->andFilterWhere(['like', 'lastUpdated', $this->lastUpdated])
            ->andFilterWhere(['like', 'href', $this->href]);

        return $dataProvider;
    }

    public function childHealth($params)
    {
        $query = DHISObject::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'object_id' => $this->object_id,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'created', $this->created])
            ->andFilterWhere(['like', 'lastUpdated', $this->lastUpdated])
            ->andFilterWhere(['like', 'href', '/organisationUnits/']);

        $query->andOnCondition("id in (select organisation_unit_id from organisation_unit_data_set where data_set_id='BfMAe6Itzgt')");
        $query->orderBy('name');

        return $dataProvider;
    }
}
