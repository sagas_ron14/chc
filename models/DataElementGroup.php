<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_element_group".
 *
 * @property integer $object_id
 * @property string $data_element_group_set_id
 * @property string $shortName
 * @property string $displayName
 * @property string $displayShortName
 */
class DataElementGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_element_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_element_group_set_id', 'shortName', 'displayName', 'displayShortName'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'object_id' => 'Object ID',
            'data_element_group_set_id' => 'Data Element Group Set ID',
            'shortName' => 'Short Name',
            'displayName' => 'Display Name',
            'displayShortName' => 'Display Short Name',
        ];
    }
}
