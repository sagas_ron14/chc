<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organisation_unit_data_set".
 *
 * @property integer $object_id
 * @property string $data_set_id
 * @property string $organisation_unit_id
 */
class OrganisationUnitDataSet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organisation_unit_data_set';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_set_id', 'organisation_unit_id'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'object_id' => 'Object ID',
            'data_set_id' => 'Data Set ID',
            'organisation_unit_id' => 'Organisation Unit ID',
        ];
    }

    public static function record($unit_id, $dataSetObject)
    {
        $setData = [ 'data_set_id'=>$dataSetObject->id, 'organisation_unit_id'=>$unit_id];
        if(null == self::find()->where($setData)->one()){
            $record = new self;
            $record->attributes = (array)$dataSetObject;
            $record->save();
            DataSet::saveFromDHISArray($dataSetObject);
        };
    }
}
