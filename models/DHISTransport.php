<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 6/21/15
 * Time: 11:30 AM
 */

namespace app\models;

use linslin\yii2\curl\Curl;

class DHISTransport {

    private static $username = 'admin';
    private static $password = 'district';

    public static function fetch($href, $verbose = false){

        if($href==''){
            echo $verbose ? "\n400: invalid" : '';
            return ['code'=>400, 'response'=>'invalid request'];
        }

        $pathParts = self::getPathComponents($href);

        $curl = new Curl();
        $curl->setOption(CURLOPT_USERPWD, self::$username.':'.self::$password);
        $response = $curl->get($href);

        $result = ['code'=>$curl->responseCode, 'response'=>null];
        echo $verbose ? "\n".$curl->responseCode.": Ok [{$pathParts['3']}:{$pathParts['4']}]" : '';

        if($curl->responseCode == 200)
            $result['response']=$response;

        return $result;
    }

    public static function fetchByParams($group, $id, $verbose = false){
        if(in_array(null, [$group, $id]))
            return ['code'=>400, 'response'=>'invalid request'];

        return self::fetch("https://apps.dhis2.org/demo/api/$group/$id", $verbose);
    }

    public static function getPathComponents($uri){
        list($protocol, $url) = explode('://', $uri);
        return explode('/', $url);
    }

}