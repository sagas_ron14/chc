<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DataSet;

/**
 * DataSetSearch represents the model behind the search form about `app\models\DataSet`.
 */
class DataSetSearch extends DataSet
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id'], 'integer'],
            [['id', 'code', 'shortName', 'displayName', 'displayShortName', 'periodType', 'legend_set_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataSet::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'object_id' => $this->object_id,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'shortName', $this->shortName])
            ->andFilterWhere(['like', 'displayName', $this->displayName])
            ->andFilterWhere(['like', 'displayShortName', $this->displayShortName])
            ->andFilterWhere(['like', 'periodType', $this->periodType])
            ->andFilterWhere(['like', 'legend_set_id', $this->legend_set_id]);

        return $dataProvider;
    }
}
