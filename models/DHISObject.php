<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dhis_object".
 *
 * @property integer $object_id
 * @property string $id
 * @property string $name
 * @property string $code
 * @property string $created
 * @property string $lastUpdated
 * @property string $href
 */
class DHISObject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dhis_object';
    }

    public static function saveObject($attributes)
    {
        $object = self::find()->where(['id'=>$attributes['id']])->one();

        if($object == null){
            $object = new self;
            $object->attributes = $attributes;
            $object->save();
        }

        return $object;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id'], 'integer'],
            [['id', 'name', 'code', 'created', 'lastUpdated', 'href'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'object_id' => 'Object ID',
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'created' => 'Created',
            'lastUpdated' => 'Last Updated',
            'href' => 'Href',
        ];
    }
}
