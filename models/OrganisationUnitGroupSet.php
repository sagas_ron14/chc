<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organisation_unit_group_set".
 *
 * @property integer $object_id
 * @property string $id
 * @property string $group_id
 */
class OrganisationUnitGroupSet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organisation_unit_group_set';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'group_id'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'object_id' => 'Object ID',
            'id' => 'ID',
            'group_id' => 'Group ID',
        ];
    }

    public static function addGroup($group_id, $organisationUnitGroupSet)
    {
        if(null == self::find()->where(['id'=>$organisationUnitGroupSet['id']])->one()){
            DHISObject::saveObject($organisationUnitGroupSet);
            $set = new self;
            $set->id = $organisationUnitGroupSet['id'];
            $set->group_id = $group_id;
            $set->save();
        }
    }

}
